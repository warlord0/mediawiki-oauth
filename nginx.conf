# Sample Nginx Config

geo $maintenance {
    default off;
    1192.168.0.0/24 off;
}

upstream server-maintenance {
    server 127.0.0.1:12399;
}

upstream server-wiki {
    server 127.0.0.1:12380 weight=1 fail_timeout=30s;
}

proxy_cache_path cache/ keys_zone=auth_cache:10m;

server {
    listen	     80;
    server_name  wiki;

    location / {
        return 301 https://wiki$request_uri;
    }
    
    location ~ /.well-known {
        allow all;
    }	
}

server {
    listen 	    443 ssl http2;
    server_name wiki;

    ssl_certificate_key      /etc/letsencrypt/live/wiki/privkey.pem;
    ssl_certificate          /etc/letsencrypt/live/wiki/fullchain.pem;
    ssl_trusted_certificate  /etc/letsencrypt/live/wiki/fullchain.pem;

    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # modern configuration
    ssl_protocols TLSv1.3;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # Specifies the maximum accepted body size of a client request,
    # as indicated by the request header Content-Length.
    client_max_body_size 200m;

    # increase proxy buffer to handle some Odoo web requests
    proxy_buffers 16 64k;
    proxy_buffer_size 128k;

    proxy_connect_timeout       60;
    proxy_read_timeout          4800;

    if ($maintenance = on) {
        return 503;
    }

    error_page 503 @maintenance;

    location @maintenance {
        rewrite ^(.*)$ / break;
        proxy_pass http://server-maintenance;
    }

    location / {
        proxy_pass http://server-wiki;
        # force timeouts if the backend dies
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;

        # set headers
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Server-Select $scheme;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Url-Scheme: $scheme;
        proxy_set_header Host $host;
        proxy_http_version 1.1;
        proxy_cookie_path / "/; HTTPOnly; Secure";

        # by default, do not forward anything
        proxy_redirect off;
    }

}
