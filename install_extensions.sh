#!/bin/bash

# This file downloads the latest extension listed in EXT=

source .env

# EXT="LDAPProvider LDAPAuthentication2 PluggableAuth" # LDAP
EXT="PluggableAuth OpenIDConnect" # OIDc Connect

if [[ -d "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/extensions" ]]; then
  
  pushd "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/extensions"
  ALLEXT=`curl https://extdist.wmflabs.org/dist/extensions/ | awk 'BEGIN { FS = "\""  } ; {print $2}'`
  for i in $EXT; do \
    echo Processing $EXT
    FILENAME=`echo "$ALLEXT" | grep ^${i}-master`
    wget --quiet https://extdist.wmflabs.org/dist/extensions/${FILENAME}
    echo "Extracting ${FILENAME} ..."
    tar xzf ${FILENAME}
    rm -f ${FILENAME}
  done

  # Install the pivot theme
  cd ../skins
  [ ! -d 'pivot' ] && git clone https://github.com/Hutchy68/pivot.git -b login-improvements pivot   

  popd

  # cp ldapprovider.json "${CONTAINER_VOLUMES}/${SERIAL}/wiki/" # LDAP
  cp compose.local.json "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/" # OIDC Connect
  cp CustomSettings.php "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/"

  if ! grep -q "@require_once('CustomSettings.php');" ${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/LocalSettings.php; then
    echo "@require_once('CustomSettings.php');" >> ${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/LocalSettings.php
  fi

  mkdir -p "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/skins/common/images"
  cp logo_default.png "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/skins/common/images"

else

  echo "${CONTAINER_VOLUMES}/${SERIAL}/wiki/mediawiki/extensions - Not Found!"

fi