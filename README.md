# MediaWiki Docker Implementation

Mediawiki with OAuth2/OIDc support for Keycloak

## Installing

Edit the `ldapprovider.json` file to suit your environment.

Create the data folders and grant permissions - __WARNING!__ Will remove existing data:

```shell
sudo ./reset.sh
```

Start the container set:

```shell
docker-compose build && \
docker-compose up -d
```

Once mediawiki is started run the script `install_extensions.sh` using sudo. This will download the latest LDAP extensions (and any others you might add into the EXT= line) and then copy in the `CustomSettings.php` and `ldapprovider.json` files.

The script also download and installs the pivot theme and copies the `CustomSettings.php` file into the right place. 

```shell
sudo ./install_extensions.php
```

Then restart the container set:

```shell
docker-compose down && \
docker-compose up -d
```

### Configuration

As `LocalSettings.php` is located in the container we should not edit this file and instead make changes to `CustomSettings.php` to override the settings. This ensures we keep all our own settings in the event of a container update changing the `LocalSettings.php`.

After you make changes to `CustomSettings.php` you should run the script to copy it in.

## Notes

At this moment yoy need to create the `mysql` volume folder and give it full permissions (`chmod 777 mysql`) otherwise MariaDB will fail to start and create the database. We need to investigate what permissions are actually required and reduce them.

### Imagemagic and SVG support

In order to convert images across many formats the `imagemagick` package is required. To install it I created a Dockerfile that causes mediawiki to be built from the `bitnami/mediawiki:latest` image and add it into the build.

For `imagemagick` to convert SVG files to thumbnails you need to install the `librsvg2-bin` package. Which I also added to the `Dockerfile`.

The installation process needs to be `RUN` as root so the `Dockerfile` switches `USER root` and then back to the non-root `USER 1001`.

(see [Dockerfile](./build/Dockerfile))

### LDAP Support

LDAP is built in but I commented most of it out as I wanted to use OAuth2 instead. You can't have both.

You'll find the commented out bits for LDAP in the `install_extensions.sh` where I set `EXT=` and by not copying the `ldapprovider.json` file. Also in the `CustomSettings.php` file where there is a lot of extension loading and config commented out grouped together.

You'll also want to modify the `ldapprovider.json` to suit your LDAP setup.

### OAuth2 Support

This is fully enabled in the `CustomSettings.php` and has config that is used on my local installation. You'll want to set this to match the client setting in your keycloak setup.

Things to change:

```php
$wgOpenIDConnect_Config['https://KEYCLOAK/auth/realms/REALM/'] = [
  'name' => 'Keycloak',
  'clientID' => 'mediawiki',
  'clientsecret' => 'SuperSecretKey',
  'scope' => [ 'openid', 'email',  'profile' ]
];
```

Change the KEYCLOAK server name and REALM. Set the client id and secret as per your keycloak settings.
