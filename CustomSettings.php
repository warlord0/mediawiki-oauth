<?php

// error_reporting( -1 );
// ini_set( 'display_errors', 1 );

// $wgShowExceptionDetails = true;
// $wgDebugToolbar = true;
// $wgShowDebug = true;
// $wgDevelopmentWarnings = true;

# Setup the pivot responsive kin

wfLoadSkin( 'pivot' );

# Pivot is specific, so lets disable other skins
$wgSkipSkins = array( 'chick', 'cologneblue', 'modern', 'myskin', 'nostalgia', 'simple', 'standard', 'filament', 'monobook', 'vector' );

$wgDefaultSkin = 'pivot';

# MySQL specific settings
$wgDBprefix = "";

# LDAP Authentication
// wfLoadExtension( 'PluggableAuth' ); 
// wfLoadExtension( 'LDAPProvider' );
// wfLoadExtension( 'LDAPAuthentication2' );
// wfLoadExtension( 'LDAPAuthorization' );

// $wgPluggableAuth_ButtonLabel = "Login Using LDAP";

// $LDAPProviderDefaultDomain = "domain";
// $LDAPProviderDomainConfigs = "/bitnami/ldapprovider.json";

# Private Wiki
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['read'] = false;

$wgServer = WebRequest::detectServer();

$wgLogo = "$wgStylePath/skins/common/images/logo_default.png";

# use ImageMagick in mediawiki
$wgUseImageResize = true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";

# For SVG image support
$wgFileExtensions = array_merge( $wgFileExtensions,
    array( 'svg' )
);
$wgAllowTitlesInSVG = true;
$wgSVGConverter = 'ImageMagick';
$wgSVGConverters = array(
  'ImageMagick' => '"/usr/bin/convert" -background white -geometry $width $input $output',
);

// $wgCookieHttpOnly = true;
// $wgCookieSecure = true;

# Added because of logon issue:
// There seems to be a problem with your login session; this action has been canceled as a precaution against session hijacking. Please resubmit the form.
$wgObjectCacheSessionExpiry = 86400;

# OIDC connect

wfLoadExtension( 'PluggableAuth' ); 
wfLoadExtension( 'OpenIDConnect' );

$wgOpenIDConnect_Config['https://KEYCLOAK/auth/realms/REALM/'] = [
  'name' => 'Keycloak',
  'clientID' => 'mediawiki',
  'clientsecret' => 'SuperSecretKey',
  'scope' => [ 'openid', 'email',  'profile' ]
];

$wgGroupPermissions['*']['autocreateaccount'] = true;

$wgPluggableAuth_ButtonLabelMessage = 'Single Sign On';
