#!/bin/bash

source .env

docker-compose down

rm -rf ${CONTAINER_VOLUMES}/${SERIAL}/{mysql,socket,wiki}
mkdir -p ${CONTAINER_VOLUMES}/${SERIAL}/{mysql,socket,wiki}
chmod 777  ${CONTAINER_VOLUMES}/${SERIAL}/{mysql,socket,wiki}